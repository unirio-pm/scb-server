package scb.server;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
//import java.util.concurrent.atomic.AtomicInteger;

public class ServerService {

    private static Map<Integer, Server> totens = new HashMap<>();
    //private static AtomicInteger lastId;

    static {
    	totens.put(0, new Server("0", "1234", "daisdjiajsdisa"));
    	totens.put(1, new Server("0", "1234", "daisdjiajsdisa"));
    	totens.put(2, new Server("0", "1234", "daisdjiajsdisa"));
    	totens.put(3, new Server("0", "1234", "daisdjiajsdisa"));
        //lastId = new AtomicInteger(totens.size());
    }

	/*
	 * public static void save(int numero) { int id = lastId.incrementAndGet();
	 * totens.put(id, new Teste(id, numero)); }
	 */

    public static Collection<Server> getAll() {
        return totens.values();
    }

	/*
	 * public static void update(int totemId, int numero) { totens.put(totemId, new
	 * Teste(totemId, numero)); }
	 */

    public static Server findById(int totemId) {
        return totens.get(totemId);
    }

    public static void delete(int totemId) {
    	totens.remove(totemId);
    }

}
