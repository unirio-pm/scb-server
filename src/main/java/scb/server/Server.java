package scb.server;

public class Server {
	public final String id;
    public final String senha;
    public final String email;

    public Server(String id, String senha, String email) {
        this.id = id;
        this.senha = senha;
        this.email = email;
    }
}
