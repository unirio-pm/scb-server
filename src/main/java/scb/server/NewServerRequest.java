package scb.server;

public class NewServerRequest {
	public String id;
    public String senha;
    public String email;

    public NewServerRequest() {
    }

    public NewServerRequest(String senha, String email) {
        this.senha = senha;
        this.email = email;
    }
}
