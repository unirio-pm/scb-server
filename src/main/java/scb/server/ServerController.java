package scb.server;

import io.javalin.http.Context;

public class ServerController {

    public static void aloMundo(Context ctx) {		 
		ctx.status(200);		 
		ctx.result("Al� Mundo!");
    }
    
    public static void aloNome(Context ctx) {		 
		String nome = ctx.pathParam("nome");
		
    	ctx.status(200);		 
		ctx.result("Al� " + nome + "!");
    }
}